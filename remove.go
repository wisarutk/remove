package remove

import (
	"fmt"

	"github.com/helloyi/go-sshclient"
	"github.com/kpango/glg"
)

type InputParam struct {
	Hostname   string `json:"hostname"`
	Username   string `json:"username"`
	Password   string `json:"password"`
	AbPathFile string `json:"ab_path_file"`
}

func Remove(host, username, password, abPath string) error {
	client, err := sshclient.DialWithPasswd(host+":22", username, password)
	if err != nil {
		return err
	}
	defer client.Close()
	//"mv " + sourcePath + " " + destPath

	glg.Info("remove file:", host, abPath)
	out, err := client.Cmd("sudo rm -f " + abPath).SmartOutput()
	if err != nil {
		return err
	}
	fmt.Println(string(out))

	return nil
}
